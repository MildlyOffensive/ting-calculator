from fpdf import FPDF  # fpdf2: https://pypi.org/project/fpdf2/
import logging

log = logging.getLogger('')


class ManagePDF:
    """
    Dispatch individual and group billings to respective class in order to generate PDFs.
    This is necessary as the header in FPDF cannot be called from within the same instance, and needs to be called from
    a unique individual instance.
    """
    def __init__(self, individual_values, group_values):
        self.individual_values = individual_values
        self.group_values = group_values

        # Contains the billings to be billed individually and as groups.
        self.group_billing_values = None
        self.individual_billing_values = None

        # Compute the billing groups
        self.compute_billing_group()

        # Write PDF files for each of the individuals to be billed.
        for entry in self.individual_billing_values:
            GenerateIndividualPDF(entry, self.group_values)

        # Write PDF files for each of the groups to be billed.
        for entry in self.group_billing_values:
            GenerateGroupPDF(self.group_billing_values[entry], self.group_values)

    def compute_billing_group(self):
        """
        Split up devices to be billed individually, and group together devices to be billed under a single group.
        Group billings are for the situation where an individual payer has multiple devices.
        :return:
        """
        individual = []
        group = {}

        # Divide up the entries that are to be billed individually from those that are to be billed as a group.
        for entry in self.individual_values:
            # Group all the entries that are to be billed individually.
            if entry['billing group'] == '' or entry['billing group'] == '0':
                individual.append(entry)

            # Group all the entries that are to be billed as a group.
            if entry['billing group'] != '' and entry['billing group'] != '0' and entry['billing group'] != None:
                # Append a new entry to the inner list in the dictionary if one already exists.
                if entry['billing group'] in group:
                    temp_group_entry = group.get(entry['billing group'])
                    temp_group_entry.append(entry)

                    group[entry['billing group']] = temp_group_entry

                # Add a new entry to the dictionary if one doesn't exist yet.
                else:
                    group[entry['billing group']] = [entry]

        self.individual_billing_values = individual
        self.group_billing_values = group

        log.debug('compute_billing_group(), self.individual_billing_values: %s', individual)
        log.debug('compute_billing_group(), self.group_billing_values: %s', group)


class GenerateIndividualPDF(FPDF):
    """
    Generate a PDF file for devices that are billed individually.
    """
    def __init__(self, individual_billing_values, group_values):
        """
        Initialization of object.
        :param individual_values: A dictionary containing the individual_values we want to apply.
        """

        self.group_values = group_values
        self.document = None
        self.month = None  # The billing month.  Computed in format_date() and assigned here.
        self.year = None  # The billing year.  Computed in format_date() and assigned here.

        # Contains the billings to be billed individually and as groups.
        self.group_billing_values = None
        self.individual_billing_values = individual_billing_values

        # Override FPDF defaults
        super(FPDF, self).__init__()
        self.figcount = 1

        # Initialize FPDF document
        FPDF.__init__(self, 'P', 'mm', 'letter')

        # Write PDF files for each of the individuals to be billed.
        self.format_date()  # Compute billing dates.
        self.create_document()
        self.add_text()
        self.add_individual_text(self.individual_billing_values)
        self.add_group_totals_text()
        self.alias_nb_pages()  # fill in number of pages before writing PDF.
        self.output_pdf(self.individual_billing_values)
        self.document = None

    def header(self):
        # Position at 1.5 cm from top
        self.set_y(15)
        self.set_font('Arial', '', 15)

        self.cell(40, 10, 'Ting Bill', 1, 0, 'C')
        self.cell(0, 10, 'Statement Ending: ' + self.month + ' ' + self.year, 0, 1, 'R')

        # Line break
        self.ln(5)

    # Page footer
    def footer(self):
        # Position at 1.5 cm from bottom
        self.set_y(-15)
        # Arial italic 8
        self.set_font('Arial', 'I', 8)
        # Page number
        self.cell(0, 10, 'Page ' + str(self.page_no()) + '/{nb}', 0, 0, 'C')

    def create_document(self):
        """
        Create PDF document and page layout.
        :return:
        """

        self.set_margins(25, 25, 25)
        self.add_page()
        self.set_font('Arial', size=12)

    def add_text(self):
        """
        Layout and write text to cells.
        :return: None
        """

    def add_individual_text(self, entry):
        """
        Add the values for each individual billed device to it's respective PDF.
        :param entry: Dictionary containing the values for a single device to be billed individually.
        :return:
        """

        # Individual Device Bill Section
        self.cell(50, 10, 'Individual Device Bill:', 1, 1, 'C')
        self.cell(40, 7, 'Device Name: ' + entry['user'], 0, 1, 'L')
        self.cell(40, 7, 'ICCID: ' + entry['iccid'], 0, 1, 'L')
        self.cell(40, 7, 'Megabytes: ' + str(
            round(entry['Kilobytes'] / 1024, 2)) + 'MB', 0, 1, 'L')
        self.cell(40, 7, 'Messages: ' + str(entry['messages']), 0, 1, 'L')
        self.cell(40, 7, 'Minutes: ' + str(entry['minutes']), 0, 1, 'L')
        self.cell(40, 7, 'Percent of total group Megabytes: ' +
                           str(round(entry['Kilobytes fraction'] * 100, 2)) + '%', 0, 1, 'L')
        self.cell(40, 7, 'Percent of total group Messages: ' +
                           str(round(entry['messages fraction'] * 100, 2)) + '%', 0, 1, 'L')
        self.cell(40, 7, 'Percent of total group Minutes: ' +
                           str(round(entry['minutes fraction'] * 100, 2)) + '%', 0, 1, 'L')
        self.cell(40, 7, 'Megabytes Fee: $' + str('{:.2f}'.format(
            round(entry['Kilobytes price'], 2))), 0, 1, 'L')
        self.cell(40, 7, 'Messages Fee: $' + str('{:.2f}'.format(
            round(entry['messages price'], 2))), 0, 1, 'L')
        self.cell(40, 7, 'Minutes Fee: $' + str('{:.2f}'.format(
            round(entry['minutes price'], 2))), 0, 1, 'L')
        self.cell(40, 7, 'Device Fee: $' + str('{:.2f}'.format(
            round(float(entry['device fee'])), 2)), 0, 1, 'L')
        self.cell(40, 7, 'Device Tax: $' + str('{:.2f}'.format(
            round(entry['device tax'], 2))), 0, 1, 'L')
        self.cell(40, 7, 'Total Fee: $' + str('{:.2f}'.format(
            round(entry['total price'], 2))), 0, 1, 'L')
        self.cell(40, 10, '', 0, 1, 'L')  # Blank Spacer

    def add_group_totals_text(self):
        """
        Writes the cumulative account totals for all devices to the PDF file for reference.
        :return:
        """

        # Group usage section
        self.cell(40, 10, 'Group Usage:', 1, 1, 'C')
        self.cell(40, 7, 'The total values for all users under the group billing account. ', 0, 1, 'L')
        self.cell(40, 7, 'Total Devices: ' + str(self.group_values['total devices']), 0, 1, 'L')
        self.cell(40, 7, 'Total Megabytes: ' + str(
                           round(self.group_values['total Kilobytes'] / 1024, 2)) + 'MB', 0, 1, 'L')
        self.cell(40, 7, 'Total Messages: ' + str(self.group_values['total messages']), 0, 1, 'L')
        self.cell(40, 7, 'Total Minutes: ' + str(self.group_values['total minutes']), 0, 1, 'L')
        self.cell(40, 7, 'Total Megabytes Fee: $' + str('{:.2f}'.format(
                           round(self.group_values['total megabytes fee'], 2))), 0, 1, 'L')
        self.cell(40, 7, 'Total Messages Fee: $' + str('{:.2f}'.format(
                           round(self.group_values['total messages fee'], 2))), 0, 1, 'L')
        self.cell(40, 7, 'Total Minutes Fee: $' + str('{:.2f}'.format(
                           round(self.group_values['total minutes fee'], 2))), 0, 1, 'L')
        self.cell(40, 7, 'Total Device Fees: $' + str('{:.2f}'.format(
                           round(self.group_values['total device fee']))), 0, 1, 'L')
        self.cell(40, 7, 'Total Taxes & Regulatory Fees: $' + str('{:.2f}'.format(
                           round(self.group_values['total taxes'], 2))), 0, 1, 'L')
        self.cell(40, 7, 'Total Bill: $' + str('{:.2f}'.format(
                           round(self.group_values['total bill'], 2))), 0, 1, 'L')
        self.cell(40, 10, '', 0, 1, 'L')  # Blank Spacer

    # User bill
    def output_pdf(self, entry):
        """
        Output PDF for individual device/user.
        :return: None.
        """

        try:
            self.output('output/' + entry['user'] + ' - ' + self.month + ' ' + self.year + '.pdf')  # Windows format.
        except OSError:
            log.exception("PDF output file is not writeable.")

    def format_date(self):
        """
        Formats the date received from the group dictionary into the format for the PDF layout.
        :return:
        """

        self.month = str(self.group_values['last transaction date'].strftime("%B"))
        self.year = str(self.group_values['last transaction date'].year)


class GenerateGroupPDF(FPDF):
    """
    Generate a single PDF document for a billing group.
    """
    def __init__(self, group_billing_values, group_values):
        """
        Initialization of object.
        :param individual_values: A dictionary containing the individual_values we want to apply.
        """

        self.group_values = group_values
        self.document = None
        self.month = None  # The billing month.  Computed in format_date() and assigned here.
        self.year = None  # The billing year.  Computed in format_date() and assigned here.

        # Contains the billings to be billed individually and as groups.
        self.group_billing_values = group_billing_values

        # Override FPDF defaults
        super(FPDF, self).__init__()
        self.figcount = 1

        # Initialize FPDF document
        FPDF.__init__(self, 'P', 'mm', 'letter')

        # Write PDF files for each of the groups to be billed.
        self.format_date()
        self.create_document()
        self.add_billing_group_totals_text(self.group_billing_values)
        self.add_page()
        self.add_billing_group_text(self.group_billing_values)
        self.add_group_totals_text()
        self.alias_nb_pages()
        self.output_pdf_group(self.group_billing_values)

        self.document = None

    def header(self):
        # Position at 1.5 cm from top
        self.set_y(15)
        self.set_font('Arial', '', 15)
        self.cell(40, 10, 'Ting Bill', 1, 0, 'C')
        self.cell(0, 10, 'Statement Ending: ' + self.month + ' ' + self.year, 0, 1, 'R')
        # Line break
        self.ln(5)

    # Page footer
    def footer(self):
        # Position at 1.5 cm from bottom
        self.set_y(-15)
        # Arial italic 8
        self.set_font('Arial', 'I', 8)
        # Page number
        self.cell(0, 10, 'Page ' + str(self.page_no()) + '/{nb}', 0, 0, 'C')

    def create_document(self):
        """
        Create PDF document and page layout.
        :return:
        """

        self.set_margins(25, 25, 25)
        self.add_page()
        self.set_font('Arial', size=12)

    def add_billing_group_text(self, entry):
        """
        Add the values from each individual device in the billing group to the PDF.
        :param entry: list containing dictionaries for each device in the billing group.
        :return:
        """

        # Write values for each individual device onto a page in the group billing PDF document.
        for listing in entry:
            # Individual Device Bill Section
            self.cell(50, 10, 'Individual Device Bill:', 1, 1, 'C')
            self.cell(40, 7, 'Device Name: ' + listing['user'], 0, 1, 'L')
            self.cell(40, 7, 'ICCID: ' + listing['iccid'], 0, 1, 'L')
            self.cell(40, 7, 'Megabytes: ' + str(
                round(listing['Kilobytes'] / 1024, 2)) + 'MB', 0, 1, 'L')
            self.cell(40, 7, 'Messages: ' + str(listing['messages']), 0, 1, 'L')
            self.cell(40, 7, 'Minutes: ' + str(listing['minutes']), 0, 1, 'L')
            self.cell(40, 7, 'Percent of total group Megabytes: ' +
                               str(round(listing['Kilobytes fraction'] * 100, 2)) + '%', 0, 1, 'L')
            self.cell(40, 7, 'Percent of total group Messages: ' +
                               str(round(listing['messages fraction'] * 100, 2)) + '%', 0, 1, 'L')
            self.cell(40, 7, 'Percent of total group Minutes: ' +
                               str(round(listing['minutes fraction'] * 100, 2)) + '%', 0, 1, 'L')
            self.cell(40, 7, 'Megabytes Fee: $' + str('{:.2f}'.format(
                round(listing['Kilobytes price'], 2))), 0, 1, 'L')
            self.cell(40, 7, 'Messages Fee: $' + str('{:.2f}'.format(
                round(listing['messages price'], 2))), 0, 1, 'L')
            self.cell(40, 7, 'Minutes Fee: $' + str('{:.2f}'.format(
                round(listing['minutes price'], 2))), 0, 1, 'L')
            self.cell(40, 7, 'Device Fee: $' + str('{:.2f}'.format(
                round(float(listing['device fee'])), 2)), 0, 1, 'L')
            self.cell(40, 7, 'Device Tax: $' + str('{:.2f}'.format(
                round(listing['device tax'], 2))), 0, 1, 'L')
            self.cell(40, 7, 'Total Fee: $' + str('{:.2f}'.format(
                round(listing['total price'], 2))), 0, 1, 'L')
            self.cell(40, 10, '', 0, 1, 'L')  # Blank Spacer
            self.add_page()

    def add_billing_group_totals_text(self, entry):
        """
        Write the totals for the billing group into the PDF document.
        :param entry: list containing dictionaries for each device in the billing group.
        :return:
        """

        billing_group_totals = {'Kilobytes': 0,
                                'messages': 0,
                                'minutes': 0,
                                'Kilobytes fraction': 0,
                                'messages fraction': 0,
                                'minutes fraction': 0,
                                'Kilobytes price': 0,
                                'messages price': 0,
                                'minutes price': 0,
                                'device fee': 0,
                                'device tax': 0,
                                'total price': 0}

        # Sum the values from all the devices in the billing group and write to temporary dictionary
        # billing_group_totals
        for listing in entry:
            billing_group_totals['Kilobytes'] = billing_group_totals['Kilobytes'] + listing['Kilobytes']
            billing_group_totals['messages'] = billing_group_totals['messages'] + listing['messages']
            billing_group_totals['minutes'] = billing_group_totals['minutes'] + listing['minutes']
            billing_group_totals['Kilobytes fraction'] = \
                billing_group_totals['Kilobytes fraction'] + listing['Kilobytes fraction']
            billing_group_totals['messages fraction'] = \
                billing_group_totals['messages fraction'] + listing['messages fraction']
            billing_group_totals['minutes fraction'] = \
                billing_group_totals['minutes fraction'] + listing['minutes fraction']
            billing_group_totals['Kilobytes price'] = \
                billing_group_totals['Kilobytes price'] + listing['Kilobytes price']
            billing_group_totals['messages price'] = billing_group_totals['messages price'] + listing['messages price']
            billing_group_totals['minutes price'] = billing_group_totals['minutes price'] + listing['minutes price']
            billing_group_totals['device fee'] = billing_group_totals['device fee'] + float(listing['device fee'])
            billing_group_totals['device tax'] = billing_group_totals['device tax'] + listing['device tax']
            billing_group_totals['total price'] = billing_group_totals['total price'] + listing['total price']

        # Add values to PDF.
        title_width = len('Total Bill for all Devices: ' + str(entry[0]['billing group']))
        self.cell((title_width + 40), 10, 'Total Bill for all Devices: ' + str(entry[0]['billing group'])
                           , 1, 1, 'C')
        self.cell(40, 7, 'Megabytes: ' + str(
            round(billing_group_totals['Kilobytes'] / 1024, 2)) + 'MB', 0, 1, 'L')
        self.cell(40, 7, 'Messages: ' + str(billing_group_totals['messages']), 0, 1, 'L')
        self.cell(40, 7, 'Minutes: ' + str(billing_group_totals['minutes']), 0, 1, 'L')
        self.cell(40, 7, 'Percent of total group Megabytes: ' +
                           str(round(billing_group_totals['Kilobytes fraction'] * 100, 2)) + '%', 0, 1, 'L')
        self.cell(40, 7, 'Percent of total group Messages: ' +
                           str(round(billing_group_totals['messages fraction'] * 100, 2)) + '%', 0, 1, 'L')
        self.cell(40, 7, 'Percent of total group Minutes: ' +
                           str(round(billing_group_totals['minutes fraction'] * 100, 2)) + '%', 0, 1, 'L')
        self.cell(40, 7, 'Megabytes Fee: $' + str('{:.2f}'.format(
            round(billing_group_totals['Kilobytes price'], 2))), 0, 1, 'L')
        self.cell(40, 7, 'Messages Fee: $' + str('{:.2f}'.format(
            round(billing_group_totals['messages price'], 2))), 0, 1, 'L')
        self.cell(40, 7, 'Minutes Fee: $' + str('{:.2f}'.format(
            round(billing_group_totals['minutes price'], 2))), 0, 1, 'L')
        self.cell(40, 7, 'Device Fee: $' + str('{:.2f}'.format(
            round(float(billing_group_totals['device fee'])), 2)), 0, 1, 'L')
        self.cell(40, 7, 'Device Tax: $' + str('{:.2f}'.format(
            round(billing_group_totals['device tax'], 2))), 0, 1, 'L')
        self.cell(40, 7, 'Total Fee: $' + str('{:.2f}'.format(
            round(billing_group_totals['total price'], 2))), 0, 1, 'L')
        self.cell(40, 10, '', 0, 1, 'L')  # Blank Spacer

    def add_group_totals_text(self):
        """
        Writes the cumulative account totals for all devices to the PDF file for reference.
        :return:
        """

        # Group usage section
        self.cell(40, 10, 'Group Usage:', 1, 1, 'C')
        self.cell(40, 7, 'The total values for all users under the group billing account. ', 0, 1, 'L')
        self.cell(40, 7, 'Total Devices: ' + str(self.group_values['total devices']), 0, 1, 'L')
        self.cell(40, 7, 'Total Megabytes: ' + str(
                           round(self.group_values['total Kilobytes'] / 1024, 2)) + 'MB', 0, 1, 'L')
        self.cell(40, 7, 'Total Messages: ' + str(self.group_values['total messages']), 0, 1, 'L')
        self.cell(40, 7, 'Total Minutes: ' + str(self.group_values['total minutes']), 0, 1, 'L')
        self.cell(40, 7, 'Total Megabytes Fee: $' + str('{:.2f}'.format(
                           round(self.group_values['total megabytes fee'], 2))), 0, 1, 'L')
        self.cell(40, 7, 'Total Messages Fee: $' + str('{:.2f}'.format(
                           round(self.group_values['total messages fee'], 2))), 0, 1, 'L')
        self.cell(40, 7, 'Total Minutes Fee: $' + str('{:.2f}'.format(
                           round(self.group_values['total minutes fee'], 2))), 0, 1, 'L')
        self.cell(40, 7, 'Total Device Fees: $' + str('{:.2f}'.format(
                           round(self.group_values['total device fee']))), 0, 1, 'L')
        self.cell(40, 7, 'Total Taxes & Regulatory Fees: $' + str('{:.2f}'.format(
                           round(self.group_values['total taxes'], 2))), 0, 1, 'L')
        self.cell(40, 7, 'Total Bill: $' + str('{:.2f}'.format(
                           round(self.group_values['total bill'], 2))), 0, 1, 'L')
        self.cell(40, 10, '', 0, 1, 'L')  # Blank Spacer

    def output_pdf_group(self, entry):
        """
        Output PDF for group billing.
        :return: None.
        """

        try:
            # Windows format.
            self.output('output/' + entry[0]['billing group'] + ' - ' + self.month + ' ' + self.year + '.pdf')
        except OSError:
            log.exception("PDF output file is not writeable.")

    def format_date(self):
        """
        Formats the date received from the group dictionary into the format for the PDF layout.
        :return:
        """

        self.month = str(self.group_values['last transaction date'].strftime("%B"))
        self.year = str(self.group_values['last transaction date'].year)


if __name__ == '__main__':
    pass
