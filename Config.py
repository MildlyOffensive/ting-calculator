# Python 3 script that loads and handles user specified settings from config.ini

from configobj import ConfigObj


def loadSettings():
    """
    Loads and returns settings from the specified config file as a dictionary.
    :return: dictionary of values loaded from config file.
    """

    # load the configuration file
    config = ConfigObj("config.ini")

    # load the 'IP Configuration' section of the configuration file
    user_data = config['Users']

    # add a single quote to the beginning of the ICCID number, as it is contained in the CSV file, but difficult to add
    # to the config.ini file.
    for user in user_data:
        # enter a ' character to match the CSV file listings.
        user_data.get(user)[3] = '\'' + user_data.get(user)[3]

        try:
            user_data.get(user)[4]
        except IndexError:
            # enter an empty string as a default if no group is given in the config file.
            user_data.get(user).insert(4, '')

    rates = {'rates': config['Rates']}

    print('Settings loaded.')

    return user_data, rates



if __name__ == "__main__":
    users = loadSettings()[0]
    rates = loadSettings()[1]

    print(users)
    print('Rates: ', rates['rates'])
    #print('Minutes Rate: ', settings['minutes'])

