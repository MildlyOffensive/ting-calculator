# Script to calculate Ting cellphone bill for account with multiple users.

import glob
import pandas as pd
import Config as config
import Generate_PDF as pdf
from datetime import datetime
import logging
from logging import handlers
import sys

# Setup Logging
log = logging.getLogger('')
log.setLevel(logging.DEBUG)
format = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")

# Standard output logging
ch = logging.StreamHandler(sys.stdout)
ch.setFormatter(format)
log.addHandler(ch)

# Output logs to file
fh = handlers.RotatingFileHandler('output/debug.log', maxBytes=(1048576*5), backupCount=7)
fh.setFormatter(format)
log.addHandler(fh)

log.debug('Program start.')


class TingRead:

    def __init__(self):
        self.megabytes = None  # pandas dataframe containing the contents of the megabytes csv file
        self.messages = None  # pandas dataframe containing the contents of the messages csv file
        self.minutes = None  # pandas dataframe containing the contents of the minutes csv file

        self.total_group_values = {}

        self.all_unique_iccid = None

        self.total_group_pdf_values = {}

        self.last_transaction_date = None


    def read_data(self):
        # Determine file paths for the csv files we need.
        # The issue is that the csv files have a structured name, but the exact characters can vary.
        megabytes_glob = glob.glob('csv/megabytes*.csv')
        messages_glob = glob.glob('csv/messages*.csv')
        minutes_glob = glob.glob('csv/minutes*.csv')

        # Error checking.  Make sure a CSV file was located.
        # This is necessary as glob.glob will return whatever it finds.  If a file is not found, it will return a blank
        # value, not an error.
        # Check that the megabytes data file exists.
        try:
            if megabytes_glob == []:
                raise FileNotFoundError('Unable to locate megabytes CSV file.')
        except FileNotFoundError:
            log.exception('Unable to locate megabytes CSV file.')
            exit('Unable to locate megabytes CSV file.')

        # Check that the messages file exists.
        try:
            if messages_glob == []:
                raise FileNotFoundError('Unable to locate messages CSV file.')
        except FileNotFoundError:
            log.exception('Unable to locate messages CSV file.')
            exit('Unable to locate messages CSV file.')

        # Check that the minutes file exists.
        try:
            if minutes_glob == []:
                raise FileNotFoundError('Unable to locate minutes CSV file.')
        except FileNotFoundError:
            log.exception('Unable to locate minutes CSV file.')
            exit('Unable to locate minutes CSV file.')

        # Read csv files into pandas dataframes
        self.megabytes = pd.read_csv(megabytes_glob[0], index_col=None)  # Windows
        self.messages = pd.read_csv(messages_glob[0], index_col=None)  # Windows
        self.minutes = pd.read_csv(minutes_glob[0], index_col=None)  # Windows

    def preview_data(self):
        # Preview data
        log.debug("megabytes: \n%s", self.megabytes.describe(include='all'))
        log.debug("messages: \n%s", self.messages.describe(include='all'))
        log.debug("minutes: \n%s", self.minutes.describe(include='all'))

    def sum_group_totals(self):
        """
        Sums and stores the totals group values of kilobytes, messages, and minutes.
        :return: None
        """

        # Sum and store total of data used by all users.
        self.total_group_values['total group Kilobytes'] = self.megabytes['Kilobytes'].sum()

        # Sum and store total of messages used by all users.
        self.total_group_values['total group messages'] = len(self.messages.index)

        # Sum and store total of minutes used by all users.
        self.total_group_values['total group minutes'] = self.minutes['Duration (min)'].sum()

    def get_unique_iccid(self):
        """
        Gets and saves a list of all the unique ICCID codes from the megabytes, messages, and minutes CSV files.
        It needs to be run across all files in the event that a device did not use one of the services.
        :return:None
        """

        kilobytes_iccid = self.megabytes.ICCID.unique()
        messages_iccid = self.messages.ICCID.unique()
        minutes_iccid = self.minutes.ICCID.unique()

        # get all the unique ICCID codes from Kilobytes, messages, and minuges, and add them to a list.
        self.all_unique_iccid = list(set().union(kilobytes_iccid, messages_iccid, minutes_iccid))

    def confirm_all_devices_added(self):
        """
        Confirm all ICCIDs listed in the CSV files are present in the config.ini file.
        :return: None
        """
        users_iccid = []
        for entry in users.users:
            users_iccid.append(users.users.get(entry)[3])

        for iccid in self.all_unique_iccid:
            if iccid not in users_iccid:
                log.warning("ICCID present in CSV but not in config file: %s", iccid)

        for iccid in users_iccid:
            if iccid not in self.all_unique_iccid:
                log.warning("ICCID present in config but not in CSV: %s.  %s will still be billed for the monthly "
                            "device fee and portion of taxes.", iccid, iccid)
                log.info("%s did not use any services this month.", iccid)

    def gather_total_group_pdf_values(self):
        """
        Gathers the group totals we want to write to the PDF files and adds to dictionary total_group_pdf_values.
        :return:
        """
        self.total_group_pdf_values['total devices'] = users.number_of_users
        self.total_group_pdf_values['total Kilobytes'] = self.total_group_values['total group Kilobytes']
        self.total_group_pdf_values['total messages'] = self.total_group_values['total group messages']
        self.total_group_pdf_values['total minutes'] = self.total_group_values['total group minutes']
        self.total_group_pdf_values['total megabytes fee'] = users.rate_adjusted_pricing['kilobytes']
        self.total_group_pdf_values['total messages fee'] = users.rate_adjusted_pricing['messages']
        self.total_group_pdf_values['total minutes fee'] = users.rate_adjusted_pricing['minutes']
        self.total_group_pdf_values['total device fee'] = users.number_of_users * float(users.rates['rates']['line'])
        self.total_group_pdf_values['total taxes'] = float(users.total_taxes)
        self.total_group_pdf_values['total bill'] = self.total_group_pdf_values['total megabytes fee'] + \
                                                    self.total_group_pdf_values['total messages fee'] + \
                                                    self.total_group_pdf_values['total minutes fee'] + \
                                                    self.total_group_pdf_values['total device fee'] + \
                                                    self.total_group_pdf_values['total taxes']

        # Compute the last date entered in the CSV files and add to total_group_pdf_values
        self.compute_dates()

        log.debug('self.total_group_pdf_values: %s', self.total_group_pdf_values)

    def compute_dates(self):
        """
        Compute the last transaction date from the CSV files in order to determine the billing month/year.
        Write to self.total_group_pdf_values['last transaction date'] dictionary.
        :return:
        """

        # Use try except block to account for Ting changing date format.  May need to check for more date formats.
        try:
            date_megabytes = datetime.strptime(self.megabytes['Date'].iloc[[-1]].values[0], '%B %d, %Y')
        except ValueError:
            date_megabytes = datetime.strptime(self.megabytes['Date'].iloc[[-1]].values[0], '%d-%b-%y')

        try:
            date_minutes = datetime.strptime(self.minutes['Date'].iloc[[-1]].values[0], '%B %d, %Y')
        except ValueError:
            date_minutes = datetime.strptime(self.minutes['Date'].iloc[[-1]].values[0], '%d-%b-%y')

        try:
            date_messages = datetime.strptime(self.messages['Date'].iloc[[-1]].values[0], '%d-%b-%y')
        except ValueError:
            date_messages = datetime.strptime(self.messages['Date'].iloc[[-1]].values[0], '%B %d, %Y')
            log.info('Ting changed datetime format to Month day, year')

        log.debug('date_megabytes: %s', date_megabytes)
        log.debug('date_messages: %s', date_messages)
        log.debug('date_minutes: %s', date_minutes)

        # Save the most recent transaction date from messages, minutes, and megabytes CSV files.
        self.last_transaction_date = date_megabytes
        if self.last_transaction_date < date_messages:
            self.last_transaction_date = date_messages
        if self.last_transaction_date < date_minutes:
            self.last_transaction_date = date_minutes

        # write last transaction date to dictionary total_group_pdf_values
        self.total_group_pdf_values['last transaction date'] = self.last_transaction_date
        log.debug('Last transaction date: %s', self.last_transaction_date)


class Users:
    def __init__(self):
        self.name = None
        self.phone = None
        self.nickname = None
        self.iccid = None

        self.users = None  # Will contain a dictionary of the users/devices names and attributes
        self.device_usage = []  # Will contain a list of users with service usage sums: Kilobytes, messages, minutes.

        self.rates = None  # the rate pricing data.  Consists of the current Ting rate prices.
        self.rate_adjusted_pricing = {}  # the price we will be paying for minutes, messages, and data

        self.number_of_users = 0
        self.total_taxes = None

        self.output_groups = {}

    def load_users(self):
        """
        Load user information from the config file.  Information is presented in the form of a dictionary containing:
        'user' = 'username', 'device', 'nickname', ICCID
        :return:
        """
        self.users = config.loadSettings()[0]
        self.rates = config.loadSettings()[1]

        log.debug('Log all loaded users using the value in the list stored in the dictionary.')
        for user in self.users:
            try:
                log.debug('%s', self.users.get(user)[2])
            except KeyError as e:
                log.exception('%s', e)
                continue

    def get_taxes(self):
        """
        Gets the total taxes as user input from the command line.
        TODO: Code does not exit the while loop without the float conversion.
        :return: None
        """

        total_taxes = None
        while type(total_taxes) is not float:
            try:
                total_taxes = float(input('Enter total taxes: $')) # float conversion due to code not exiting while loop
            except ValueError:
                continue

        self.total_taxes = total_taxes

    def get_number_of_users(self):
        """
        Gets the total number of users from the config file.
        Will be used to calculate tax for each user.  Will calculate incorrectly if a user is found in the usage CSV
        files but not listed in config.ini.
        :return: None
        """
        for user in self.users:
            self.number_of_users += 1

    def compute_device_usage(self):
        """
        Compute service usage for each device.
        :return:
        """
        log.debug('iterating over users')
        for key in self.users:
            # Create temporary dictionary to hold writes to main dictionary.
            # Assigns 'user' name and 'iccid' device identification numbers.
            list_entry = {'user': (self.users.get(key)[2]), 'iccid': (self.users.get(key)[3])}
            log.debug("ICCID: %s", list_entry['iccid'])

            # Either line below works to retrieve the Kilobyte data.
            list_entry['Kilobytes'] = ting_read.megabytes.loc[ting_read.megabytes['ICCID'] == (list_entry['iccid']),
                                                              'Kilobytes'].sum() # total user Kilobytes
            #list_entry['Kilobytes'] = ting_read.megabytes.groupby('ICCID')['Kilobytes'].sum()[list_entry['iccid']]

            # total user messages
            list_entry['messages'] = ting_read.messages.loc[ting_read.messages['ICCID'] ==
                                                            (list_entry['iccid'])].count()[1]  # total user messages

            # total user minutes
            list_entry['minutes'] = ting_read.minutes.loc[ting_read.minutes['ICCID'] == (list_entry['iccid']),
                                                          'Duration (min)'].sum()  # total user minutes

            # Compute percentages of total group usage for Kilobytes, messages, and minutes
            list_entry['Kilobytes fraction'] = list_entry['Kilobytes'] / \
                                               ting_read.total_group_values['total group Kilobytes']

            list_entry['messages fraction'] = list_entry['messages'] / \
                                               ting_read.total_group_values['total group messages']

            list_entry['minutes fraction'] = list_entry['minutes'] / \
                                               ting_read.total_group_values['total group minutes']

            # adds the device fee to each user entry.
            list_entry['device fee'] = users.rates['rates']['line']

            # adds the ID used as the billing group.
            try:
                list_entry['billing group'] = self.users.get(key)[4]
            except IndexError:
                # enter an empty string as a default if no group is given in the config file.
                # TODO: this except block is redundant.  It's function is performed in Config.py.
                list_entry['billing group'] = ''

            log.debug('key %s', key)
            self.device_usage.append(list_entry)

        # Compute rates for group.  Move out to own method later
        log.debug('users.rates: %s', users.rates)

        # Compute our data rates
        # TODO: tier 5 in all levels needs to account for variable/non-fixed fee
        total_group_kilobytes = int(ting_read.total_group_values['total group Kilobytes'])
        if total_group_kilobytes >= int(users.rates['rates']['data']['tier 5'][0]) * 1024:
            #print('kilobytes tier 5: $' + users.rates['rates']['data']['tier 5'][2])

            # use the total group Kilobytes and multiply by the Kilobytes beyond tier 4 and add the tier 4 price to
            # determine total tier 5 pricing
            # (total_group_kilobytes - (tier_4_upper_bound * 1024)) * tier_5_per_KB_rate) + tier_4_price
            # TODO: tier 5 pricing is untested.
            tier_5_price = (total_group_kilobytes - (int(users.rates['rates']['data']['tier 4'][1]) * 1024) *
                            users.rates['rates']['data']['tier 5'][2]) + int(users.rates['rates']['data']['tier 4'][2])

            self.rate_adjusted_pricing['kilobytes'] = tier_5_price
        elif total_group_kilobytes > int(users.rates['rates']['data']['tier 4'][0]) * 1024:
            log.debug('kilobytes tier 4: $%s', users.rates['rates']['data']['tier 4'][2])
            self.rate_adjusted_pricing['kilobytes'] = int(users.rates['rates']['data']['tier 4'][2])
        elif total_group_kilobytes > int(users.rates['rates']['data']['tier 3'][0]) * 1024:
            log.debug('kilobytes tier 3: $%s', users.rates['rates']['data']['tier 3'][2])
            self.rate_adjusted_pricing['kilobytes'] = int(users.rates['rates']['data']['tier 3'][2])
        elif total_group_kilobytes > int(users.rates['rates']['data']['tier 2'][0]) * 1024:
            log.debug('kilobytes tier 2: $%s', users.rates['rates']['data']['tier 2'][2])
            self.rate_adjusted_pricing['kilobytes'] = int(users.rates['rates']['data']['tier 2'][2])
        elif total_group_kilobytes > int(users.rates['rates']['data']['tier 1'][0]) * 1024:
            log.debug('kilobytes tier 1: $%s', users.rates['rates']['data']['tier 1'][2])
            self.rate_adjusted_pricing['kilobytes'] = int(users.rates['rates']['data']['tier 1'][2])

        # compute our messages
        total_group_messages = int(ting_read.total_group_values['total group messages'])
        if total_group_messages >= int(users.rates['rates']['messages']['tier 5'][0]):
            log.debug('messages tier 5: $%s', users.rates['rates']['messages']['tier 5'][2])

            # TODO: tier 5 pricing is untested.
            tier_5_price = (total_group_messages - (int(users.rates['rates']['messages']['tier 4'][1])) *
                            users.rates['rates']['messages']['tier 5'][2]) + \
                           int(users.rates['rates']['messages']['tier 4'][2])
            self.rate_adjusted_pricing['messages'] = tier_5_price

        elif total_group_messages > int(users.rates['rates']['messages']['tier 4'][0]):
            log.debug('messages tier 4: $%s', users.rates['rates']['messages']['tier 4'][2])
            self.rate_adjusted_pricing['messages'] = int(users.rates['rates']['messages']['tier 4'][2])
        elif total_group_messages > int(users.rates['rates']['messages']['tier 3'][0]):
            log.debug('messages tier 3: $%s', users.rates['rates']['messages']['tier 3'][2])
            self.rate_adjusted_pricing['messages'] = int(users.rates['rates']['messages']['tier 3'][2])
        elif total_group_messages > int(users.rates['rates']['messages']['tier 2'][0]):
            log.debug('messages tier 2: $%s', users.rates['rates']['messages']['tier 2'][2])
            self.rate_adjusted_pricing['messages'] = int(users.rates['rates']['messages']['tier 2'][2])
        elif total_group_messages > int(users.rates['rates']['messages']['tier 1'][0]):
            log.debug('messages tier 1: $%s', users.rates['rates']['messages']['tier 1'][2])
            self.rate_adjusted_pricing['messages'] = int(users.rates['rates']['messages']['tier 1'][2])

        # compute our minutes
        total_group_minutes = int(ting_read.total_group_values['total group minutes'])
        if total_group_minutes >= int(users.rates['rates']['minutes']['tier 5'][0]):
            log.debug('minutes tier 5: $%s', users.rates['rates']['minutes']['tier 5'][2])

            # TODO: tier 5 pricing is untested.
            tier_5_price = (total_group_minutes - (int(users.rates['rates']['minutes']['tier 4'][1])) *
                            users.rates['rates']['minutes']['tier 5'][2]) + \
                           int(users.rates['rates']['minutes']['tier 4'][2])
            self.rate_adjusted_pricing['minutes'] = tier_5_price

        elif total_group_minutes > int(users.rates['rates']['minutes']['tier 4'][0]):
            log.debug('minutes tier 4: $%s', users.rates['rates']['minutes']['tier 4'][2])
            self.rate_adjusted_pricing['minutes'] = int(users.rates['rates']['minutes']['tier 4'][2])
        elif total_group_minutes > int(users.rates['rates']['minutes']['tier 3'][0]):
            log.debug('minutes tier 3: $%s', users.rates['rates']['minutes']['tier 3'][2])
            self.rate_adjusted_pricing['minutes'] = int(users.rates['rates']['minutes']['tier 3'][2])
        elif total_group_minutes > int(users.rates['rates']['minutes']['tier 2'][0]):
            log.debug('minutes tier 2: $%s', users.rates['rates']['minutes']['tier 2'][2])
            self.rate_adjusted_pricing['minutes'] = int(users.rates['rates']['minutes']['tier 2'][2])
        elif total_group_minutes > int(users.rates['rates']['minutes']['tier 1'][0]):
            log.debug('minutes tier 1: $%s', users.rates['rates']['minutes']['tier 1'][2])
            self.rate_adjusted_pricing['minutes'] = int(users.rates['rates']['minutes']['tier 1'][2])

        log.debug("end for loop")

    def compute_dollar_price_per_user(self):
        for entry in self.device_usage:
            log.debug('Current device: %s.  ICCID: %s', entry['user'], entry['iccid'])

            # Compute the actual dollar price of Kilobytes for the user and enter into the dictionary.
            entry['Kilobytes price'] = (entry['Kilobytes fraction'] * users.rate_adjusted_pricing['kilobytes'])
            log.debug('Kilobytes price: %s', entry['Kilobytes price'])

            # Compute the actual dollar price of messages for the user and enter into the dictionary.
            entry['messages price'] = (entry['messages fraction'] * users.rate_adjusted_pricing['messages'])
            log.debug('messages price: %s', entry['messages price'])

            # Compute the actual dollar price of minutes for the user and enter into the dictionary.
            entry['minutes price'] = (entry['minutes fraction'] * users.rate_adjusted_pricing['minutes'])
            log.debug('minutes price: %s', entry['minutes price'])

            # Computes the per device tax price and enter into the dictionary.
            entry['device tax'] = float(self.total_taxes) / self.number_of_users

    def compute_total_cost_per_user(self):
        """
        Compute the total dollar cost per user and add to the dictionaries.
        :return:
        """

        # Compute the total dollar cost per user without rounding/formatting and add to the dictionary.
        for entry in self.device_usage:
            entry['total price'] = entry['Kilobytes price'] + entry['messages price'] + entry['minutes price'] + \
                                   float(users.rates['rates']['line'])

            # Round the total price to 2 decimal places and add to the dictionaries. Values may not round and
            # total precisely to 2 decimal places, which is why the raw total values is stored as well.
            entry['rounded total price'] = round(entry['total price'], 2)


if __name__ == '__main__':

    ting_read = TingRead()

    ting_read.read_data()
    # ting_read.preview_data()  # Optional for debugging purposes.

    users = Users()
    users.load_users()

    ting_read.sum_group_totals()
    log.debug('total_group_values %s', ting_read.total_group_values)

    users.get_taxes()
    users.get_number_of_users()

    users.compute_device_usage()

    users.compute_dollar_price_per_user()
    users.compute_total_cost_per_user()

    log.debug('users.device_usage: %s', users.device_usage)
    log.debug('users.users: %s', users.users)
    log.debug("users.rate_adjusted_pricing: %s", users.rate_adjusted_pricing)

    ting_read.get_unique_iccid()
    log.debug('ting_read.all_unique_iccid: %s', ting_read.all_unique_iccid)

    ting_read.confirm_all_devices_added()

    output = pd.DataFrame(users.device_usage)
    log.debug('output: \n%s', output)

    # Output computed data to CSV file.
    output.to_csv("output/Ting_usage.csv", index=False)

    ting_read.gather_total_group_pdf_values()

    # Send data to be written to PDF files.
    pdf.ManagePDF(users.device_usage, ting_read.total_group_pdf_values)

